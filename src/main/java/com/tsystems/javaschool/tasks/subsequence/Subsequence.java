package tasks.subsequence;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class Subsequence {
    public static void main(String[] args) {
        List<String> list1 = Arrays.asList("A", "B", "C","D");
        List<String> list2 = Arrays.asList("BD", "A", "ABC", "B", "M", "D", "M", "C", "DC", "D");
        boolean b = find(list1, list2);
        System.out.println(b); // Result: true

    }



    static boolean find(List<String> list1, List<String> list2) {
        List<String> list3 = new ArrayList<String>();
        for (int i = 0; i < list2.size(); i++) { //находим совпадения
            for (int j = 0; j < list1.size(); j++) {
                if (list2.get(i).equals(list1.get(j)) == true) {
                    list3.add(list1.get(j)); //массив с одинаковыми элементами
                }
            }
        }
        if (list3.size() == 0) return false;

        for (int i = 0; i < list3.size(); i++) { //удаляем повторения
            for (int j = 0; j < list3.size(); j++) {
                if ((list3.get(i).equals(list3.get(j)) && (i != j))) {
                    if ((i < j)) {
                        list3.remove(i);
                    } else {
                        list3.remove(j);
                    }
                }
            }
        }
        int amount = 0;
        for (int i = 0; i < list3.size(); i++) {
            if (list3.get(i) == list1.get(i)) {
                amount++;
            }
        }
        if (list1.size() == list3.size() && list3.size() == amount) { //определяем равен ли исходному массиву
            return true;
        } else {
            return false;
        }


    }

}






