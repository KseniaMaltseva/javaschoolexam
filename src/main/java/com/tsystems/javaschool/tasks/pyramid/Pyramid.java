package tasks.pyramid;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Pyramid {
    public static void main(String[] args) {
        input();

    }

    static void input() {
        Scanner input = new Scanner(System.in);
        ArrayList<Integer> myList = new ArrayList<Integer>();
        System.out.println("Введите числа пирамиды");
        while (myList.size() < 6) {
            int i = input.nextInt();
            myList.add(i);
        }

        Collections.sort(myList);

        for (int s : myList) {
            System.out.println(s);
        }
        int rows = 3;
        int columns = 5;
        int[][] changedArray = new int[rows][columns];

        changedArray[0][2] = myList.get(0);
        changedArray[1][1] = myList.get(1);
        changedArray[1][3] = myList.get(2);
        changedArray[2][0] = myList.get(3);
        changedArray[2][2] = myList.get(4);
        changedArray[2][4] = myList.get(5);
        for (int i = 0; i < changedArray.length; i++) {
            for (int j = 0; j < changedArray[i].length; j++) {
                System.out.print(changedArray[i][j] + "\t");
            }
            System.out.println();
        }

    }


}