package tasks.calculator;

import java.util.Stack;

public class Calculator {
    public static void main(String[] args) {

        System.out.println(calc(evaluate("7*6/2+8")));
        System.out.println(calc(evaluate("9+2-1")));

    }

    public static String evaluate(String inputStr) {
        String str;
        Stack myStack;
        str = inputStr;
        myStack = new Stack();
        String outputString = "";
        boolean b = false;
        for (int i = 0; i < str.length(); i++) {
            char curChar = str.charAt(i); //текущий символ строки

            if (!isOperator(curChar)) { //если символ операнд
                outputString += Character.toString(curChar);
                if (i == (str.length() - 1)) {
                    while (!myStack.empty()) {
                        outputString += myStack.pop(); //добавляем на верхушку стэка
                    }
                }
            } else { //если символ оператор
                if (b) {
                    if (pMin(curChar) && pMin((Character) myStack.peek())) { //если + и -
                        outputString += myStack.pop();
                        myStack.push(curChar);
                        if (i == (str.length() - 1)) {
                            while (!myStack.empty()) {
                                outputString += myStack.pop();
                            }
                        }
                    } else if (mDiv(curChar) && mDiv((Character) myStack.peek())) { //если * и /
                        outputString += myStack.pop();
                        myStack.push(curChar);
                        if (i == (str.length() - 1)) {
                            while (!myStack.empty()) {
                                outputString += myStack.pop();
                            }
                        }
                    } else if (pMin(curChar) && mDiv((Character) myStack.peek())) { //если * и -
                        outputString += myStack.pop();
                        myStack.push(curChar);
                        if (i == (str.length() - 1)) {
                            while (!myStack.empty()) {
                                outputString += myStack.pop();
                            }
                        }
                    } else if (mDiv(curChar) && pMin((Character) myStack.peek())) { //если - и /
                        myStack.push(curChar);
                        if (i == (str.length() - 1)) {
                            while (!myStack.empty()) {
                                outputString += myStack.pop();
                            }
                        }
                    } else {
                        outputString += Character.toString(curChar);
                        if (i == (str.length() - 1)) {
                            while (!myStack.empty()) {
                                outputString += myStack.pop();
                            }
                        }
                    }
                } else {
                    myStack.push(curChar);
                    b = true;
                }
            }
        }
        return outputString; //возвращаем строку в оратной польской записи
    }

    static double calc(String str) { //вычисление выражения
        double result = 0;
        Stack<Double> temp = new Stack<Double>();
        for (int i = 0; i < str.length(); i++) {
            if (Character.isDigit(str.charAt(i))) {
                temp.push((double) Character.getNumericValue(str.charAt(i)));

            } else if (isOperator(str.charAt(i))) {
                double n2 = temp.pop();
                double n1 = temp.pop();
                switch (str.charAt(i)) {
                    case '+':
                        result = n1 + n2;
                        break;
                    case '-':
                        result = n1 - n2;
                        break;
                    case '*':
                        result = n1 * n2;
                        break;
                    case '/':
                        result = n1 / n2;
                        break;
                }
                temp.push(result);
            }
        }
        return temp.peek();
    }

    boolean isParenthes(char op) {
        switch (op) {
            case '(':
                return true;
            case ')':
                return true;
            default:
                return false;
        }
    }

    public static boolean isOperator(char op) {
        switch (op) {
            case '+':
                return true;
            case '-':
                return true;
            case '/':
                return true;
            case '*':
                return true;
            default:
                return false;
        }
    }

    public static boolean pMin(char op) { // + или -
        switch (op) {
            case '+':
                return true;
            case '-':
                return true;
            default:
                return false;
        }
    }

    public static boolean mDiv(char op) {
        switch (op) {
            case '*':
                return true;
            case '/':
                return true;
            default:
                return false;
        }
    }


}

